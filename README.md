# view editing and object interpolation for faces
## CodeNeRF
Code is from https://github.com/wbjang/code-nerf and altered from my purposes.
- uses data from the celebamask-hq dataset.

## Tiny NeRF
parts of the code are taken from https://github.com/kwea123/nerf_pl and https://github.com/bmild/nerf

- Includes the trained Tiny NeRF model with estimate poses.
- uses data from mutliface

## Pose Visualisation Software
Based on an OpenCV program with edits from myself, Mirgahney Mohammed and Jingwen Wang.
Code to load real extrinsics and intrinsics of multiface dataset was altered from the code provided
by the authors of the multiface dataset https://github.com/facebookresearch/multiface and the util file is the same as the CodeNeRF util file.

- visualises a set of cameras outputted from the Pose Estimation System.

## Pose Estimation System
Heavily altered from https://www.youtube.com/watch?v=-toNMaS4SeQ

- estimates the world-to-camera matrix for an image of a face, storing both the image of the face and matrix.
