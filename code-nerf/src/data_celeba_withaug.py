import cv2
import imageio
import numpy as np
import torch
import glob, os
from utils import takeinv, center_poses

list_of_all = {}


def load_all(train_path, val_path):
    print('loaded all data')
    global list_of_all
    poses = []
    names = []
    for file in np.sort(glob.glob(train_path+"/*/*.npz")):
        npzfile = np.load(file)
        ext = npzfile['extr']
        correctionMat = np.diag(np.array([1, -1, -1, 1]))
        pose = takeinv(ext[..., :3, :])
        pose = np.concatenate([pose, np.array([0, 0, 0, 1]).reshape(1, -1)],
                              axis=0) @ correctionMat

        names.append(file)
        poses.append(pose)

    for file in np.sort(glob.glob(val_path+"/*/*.npz")):
        npzfile = np.load(file)
        ext = npzfile['extr']

        correctionMat = np.diag(np.array([1, -1, -1, 1]))
        pose = takeinv(ext[..., :3, :])
        pose = np.concatenate([pose, np.array([0, 0, 0, 1]).reshape(1, -1)],
                              axis=0) @ correctionMat

        names.append(file)
        poses.append(pose)

    poses = np.array(poses).astype('float32')
    poses = poses[..., :3, :]

    #some lines below from nerf_pl https://github.com/kwea123/nerf_pl
    poses, _ = center_poses(poses)
    near_original = 0.7
    scale_factor = near_original * 0.75
    poses[..., 3] /= scale_factor

    poses = [(np.concatenate([poses[k], np.array([0, 0, 0, 1]).reshape(1, -1)], axis=0))
             for k in range(poses.shape[0])]
    for i in range(0, len(poses)):
        list_of_all[names[i]] = poses[i]


def load_poses(pose_dir, idxs=[]):
    txtfiles = np.sort([os.path.join(pose_dir, f.name) for f in os.scandir(pose_dir)])
    posefiles = np.array(txtfiles)[idxs]
    srn_coords_trans = np.diag(np.array([1, -1, -1, 1]))
    poses = []
    for posefile in posefiles:
        pose = np.loadtxt(posefile).reshape(4, 4)
        poses.append(pose @ srn_coords_trans)
    return torch.from_numpy(np.array(poses)).float()

def load_imgs(img_dir, idxs=[]):
    files = np.sort([os.path.join(img_dir, f.name) for f in os.scandir(img_dir)])
    #print(img_dir, 'here')
    imgs = []
    #print(files, 'img')
    for file in files:
        img = imageio.imread(file, pilmode='RGB')
        img = img.astype(np.float32)
        #img = cv2.resize(img, dsize=(128, 128), interpolation=cv2.INTER_LANCZOS4)
        img = cv2.GaussianBlur(src=img, ksize=(3, 3), sigmaX=0, sigmaY=0)
        img /= 255.
        imgs.append(img)
    return torch.from_numpy(np.array(imgs)[idxs])#[TODO] check if it needs reshaping


def load_intrinsic_ext(intrinsic_path, folder_path_train, folder_path_val, idxs=[]):
    global list_of_all

    ##########
    #https://ksimek.github.io/2012/08/22/extrinsic/

    # extrinics are tranformed via https://stackoverflow.com/questions/16265714/camera-pose-estimation-opencv-pnp/20373984#20373984
    # and https://github.com/bmild/nerf/issues/141

    focal = None
    W = None
    H = None

    if len(list_of_all) == 0:
        load_all(folder_path_train, folder_path_val)

    files = np.sort([os.path.join(intrinsic_path, f.name) for f in os.scandir(intrinsic_path)])
    ext = []
    for file in files:
        ext.append(list_of_all[file])
        if(focal==None):
            npzfile = np.load(file)
            intrin = npzfile['intr']
            focal = intrin[0][0]
            H = int(focal)
            W = int(focal)
    return focal, H, W, torch.from_numpy(np.array(ext)[idxs].reshape(len(idxs), 4, 4)).float()


class SRN():
    def __init__(self, cat='', splits='train',
                 data_dir='../data/celeba-hq-aug',
                 num_instances_per_obj=1, crop_img=False):

        self.data_dir = os.path.join(data_dir, "data", splits)
        other = 'val' if splits == 'train' else 'train'
        self.other_dir = os.path.join(data_dir, "data", other)
        self.image_dir = os.path.join(data_dir, "celeba_hq", 'train')
        self.ids = [f.name for f in os.scandir(self.data_dir)]
        intids = np.sort(np.array([int(f.name) for f in os.scandir(self.data_dir)]))
        intids = [str(f) for f in intids]
        print(intids)
        self.ids = intids

        self.lenids = len(self.ids)
        self.num_instances_per_obj = num_instances_per_obj
        self.train = True if splits == 'train' else False
        self.crop_img=crop_img

    def __len__(self):
        return self.lenids

    def __getitem__(self, idx):
        obj_id = self.ids[idx]
        if self.train:
            #print(idx)
            focal, H, W, imgs, poses, instances = self.return_train_data(obj_id)

            return focal, H, W, imgs, poses, instances, idx
        else:
            focal, H, W, imgs, poses, instances = self.return_test_val_data(obj_id)
            return focal, H, W, imgs, poses, idx

    def return_train_data(self, obj_id):

        img_dir = os.path.join(self.image_dir, obj_id)
        intrinsic_path = os.path.join(self.data_dir, obj_id)

        instances = np.random.choice(1, self.num_instances_per_obj)
        focal, H, W, poses = load_intrinsic_ext(intrinsic_path, self.data_dir, self.other_dir, instances)
        imgs = load_imgs(img_dir, instances)

        if self.crop_img:
            imgs = imgs[:, 32:-32, 32:-32, :]
            H, W = H // 2, W // 2
        return focal, H, W, imgs.reshape(self.num_instances_per_obj, -1, 3), poses, instances

    def return_test_val_data(self, obj_id):

        img_dir = os.path.join(self.image_dir, obj_id)
        intrinsic_path = os.path.join(self.data_dir, obj_id)

        instances = np.array([0, 1])
        imgs = load_imgs(img_dir, instances)
        focal, H, W, poses = load_intrinsic_ext(intrinsic_path, self.data_dir, self.other_dir, instances)

        return focal, H, W, imgs.reshape(self.num_instances_per_obj, -1, 3), poses, instances