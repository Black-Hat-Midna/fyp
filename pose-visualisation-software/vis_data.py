import os
import numpy as np
import torch
import open3d as o3d
import trimesh
from tools.vis_cameras import visualize, draw_cuboid
from utils import takeinv, normalize, average_poses, center_poses
from pdb import set_trace
import glob, os
# program made with components from opencv and https://github.com/kwea123/nerf_pl

#loads real-world data from multiface dataset taken from the mutliface git and altered
def load_krt(path):
    cameras = {}

    with open(path, "r") as f:
        while True:
            name = f.readline()
            if name == "":
                break
            print(name)
            intrin = [[float(x) for x in f.readline().split()] for i in range(3)]
            dist = [float(x) for x in f.readline().split()]
            extrin = [[float(x) for x in f.readline().split()] for i in range(3)]
            f.readline()

            extrin = np.array(extrin)
            correctionMat = np.diag(np.array([1, -1, -1, 1]))

            #### transformations begin
            pose = takeinv(extrin)
            pose = pose @ correctionMat

            cameras[name[:-1]] = {
                "intrin": np.array(intrin),
                "dist": np.array(dist),
                "extrin": pose,
            }
    return cameras

def loadAllExt_Celeb(path):
    os.chdir(path)
    poses = []
    count = 0
    for file in np.sort(glob.glob("*.npz")):
        if count>-1:
            print(file)
            npzfile = np.load(file)
            intrin = npzfile['intr']
            ext = npzfile['extr']
            correctionMat = np.diag(np.array([1, -1, -1, 1]))

        #### transformations begin
            pose = takeinv(ext[..., :3, :])
            pose = np.concatenate([pose, np.array([0, 0, 0, 1]).reshape(1, -1)], axis=0)@correctionMat

            poses.append(pose)

            if count == 500:
                break
        count += 1


    #code below altered from nerf_pl https://github.com/kwea123/nerf_pl
    poses = np.array(poses).astype('float32')
    poses = poses[..., :3, :]
    poses, _ = center_poses(poses)
    near_original = 0.7#0.65#0.7#0.1 #0.9#0.65#0.7#0.1#130#120.0#0.9#0.1#130.0#111.0#5.4#0.1##0.6#900#0.1#0.6
    scale_factor = near_original * 0.75

    poses[..., 3] /= scale_factor

    return poses

if __name__ == "__main__":

    data = np.load('src/tiny_nerf_data.npz')
    images = data['images']
    poses = data['poses']
    focal = data['focal']
    print(focal)
    poses = np.array(poses).astype('float32')
    visualize(np.array(poses), [])
    """
    """
    cam_dict = load_krt('../KRT')

    c2w_list = [cam_dict[k]['extrin'] for k in cam_dict.keys()]
    c2w_list = np.array(c2w_list)

    c2w_list = np.array(c2w_list).astype('float32')

    c2w_list, pose_trans = center_poses(c2w_list)

    near_original = 900
    scale_factor = near_original * 0.75 # lines taken from https://github.com/kwea123/nerf_pl
    c2w_list[..., 3] /= scale_factor
    
    correctionMat = np.diag(np.array([1, 1, 1, 1]))
    c2w_list = [np.concatenate([c2w_list[k], np.array([0, 0, 0, 1]).reshape(1, -1)], axis=0)@correctionMat for k in range(c2w_list.shape[0])]
    c2w_list = np.array(c2w_list)

    cube = np.array([[0.5, -0.5], [0.5, -0.5], [-0.5, -1.5]])
    ls = draw_cuboid(cube)
    visualize(np.array(c2w_list)[0:2], [ls])

    path = '../data_no_gauss/train'#'../dataf/train/'#'../refdataaug_int/train/'##'../dataf/train'#'../data_int_guass/train'#'../dataf/train/'#'../data_int_guass/train'#'../refdataaug_int/train/'#'../data-7/train'#'../dataaug_int/train/'#'../paras/'#../dataaug/train/'#'../dataf/train/'#'../celeba-hq-aug/data/train/'##'../data-9/train/'##'../data-6/'#'../strangemandata/data_4/train/'#'../datanoresize/train/'#'../dataf/train'#'../celebres128x128/paras'#../paras/'#'#'../paras/' ##1a1dcd236a1e6133860800e6696b8284/'
#'../paras/'#



    poses = loadAllExt_Celeb(path)
    cube = np.array([[0.5, -0.5], [0.5, -0.5], [-1.2, -2.4]])
    correctionMat = np.diag(np.array([1, 1, 1, 1]))
    c2w_list = [(np.concatenate([poses[k] , np.array([0, 0, 0, 1]).reshape(1, -1)], axis=0)@ correctionMat) for k in
                range(poses.shape[0])]
    c2w_list = np.array(c2w_list)
    ls = draw_cuboid(cube)
    visualize(np.array(c2w_list)[2:3], [ls])

